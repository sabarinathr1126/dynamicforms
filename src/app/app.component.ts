import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl, FormGroup, UntypedFormBuilder, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'FormApp';
  formFields:any[]=[];
  form = new FormGroup({})
  constructor(private formBuilder:UntypedFormBuilder,private httpClient:HttpClient){}
  ngOnInit():void{
    this.httpClient.get<any[]>('/assets/sample.json').subscribe((formFields:any[])=>{
      for(const formField of formFields){
        this.form.addControl(formField.fieldName,new FormControl('',this.getValidator(formField)))
      }
      this.formFields=formFields;
    })

    
  }
  onSubmit():void{
    if(this.form.valid){
      console.log(this.form.value)
    }
  }

  private getValidator(formField:any): ValidatorFn[]| null{
    let validationArray=[]
    console.log(formField)
    for(let i of formField.validator){
      if(i=="email"){
        validationArray.push(Validators.email)
      }
      else if (i=="required"){
    validationArray.push( Validators.required)
      }
     
    }
    console.log(validationArray)
    return validationArray
    }

  }
